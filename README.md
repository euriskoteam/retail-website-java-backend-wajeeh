# Retail Store Discounts

A basic Java Spring Boot application for retail store discounts.

## Usage

### Clone repository

```bash
git clone https://bitbucket.org/euriskoteam/retail-website-java-backend-wajeeh
cd retail-website-java-backend-wajeeh
```

## Build

To build the project, run:

```bash
./gradlew build
```

JaCoCo now automatically creates a file `build/jacoco/test.exec` which contains the coverage statistics in binary form.

## Test Coverage

To generate an HTML coverage report, run:

```bash
./gradlew build jacocoTestReport
```

This will generate an HTML file in this path `/build/reports/jacoco/test/html/index.html`.

## UML Class Diagram

![class_diagram](class_diagram.png)
