package com.retail.demo.services;

import com.retail.demo.models.Item;
import com.retail.demo.models.ItemType;

import java.util.List;

public class BillService {

    /**
     * Returns the total price of all items after discount
     *
     * @param  items    a list of items
     * @param  discount percentage based discount
     * @return          the total price after discount
     */
    public double calculateTotalPrice(List<Item> items, double discount) {
        double totalPriceForGroceries = 0;
        double totalPriceForNonGroceries = 0;
        double totalPrice;

        for (Item item: items) {
            if (item.getType() == ItemType.GROCERY) {
                totalPriceForGroceries += item.getPrice();
            } else {
                totalPriceForNonGroceries += item.getPrice();
            }
        }

        if (discount > 0) {
            totalPriceForNonGroceries = totalPriceForNonGroceries * (1 - discount);
        }

        totalPrice = totalPriceForGroceries + totalPriceForNonGroceries;

        return totalPrice - calculateAmountBasedDiscount(totalPrice);
    }

    /**
     * Returns the total discount based on the total price ($5 discount for every $100)
     *
     * @param  totalPrice total price without discount
     * @return            total discount
     */
    public double calculateAmountBasedDiscount(double totalPrice) {
        int count = (int) totalPrice / 100;
        return count * 5;
    }
}
