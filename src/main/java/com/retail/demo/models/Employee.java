package com.retail.demo.models;

public class Employee extends User {

    public Employee(Long id, String firstName, String lastName) {
        super(id, firstName, lastName);
    }

    @Override
    public double getStoreDiscount() {
        return 0.3;
    }
}
