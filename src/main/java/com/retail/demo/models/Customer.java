package com.retail.demo.models;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

public class Customer extends User {
    private Date customerSince;

    public Customer(Long id, String firstName, String lastName) {
        super(id, firstName, lastName);
        this.customerSince = new Date();
    }

    public Date getCustomerSince() {
        return customerSince;
    }

    public void setCustomerSince(Date customerSince) {
        this.customerSince = customerSince;
    }

    @Override
    public double getStoreDiscount() {
        LocalDate dateNow = LocalDate.now();
        LocalDate date = customerSince.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Period period = Period.between(date, dateNow);
        int diffInYears = period.getYears();
        return diffInYears >= 2 ? 0.05 : 0;
    }
}
