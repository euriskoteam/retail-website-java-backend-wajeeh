package com.retail.demo.models;

public class Affiliate extends User {

    public Affiliate(Long id, String firstName, String lastName) {
        super(id, firstName, lastName);
    }

    @Override
    public double getStoreDiscount() {
        return 0.1;
    }
}
