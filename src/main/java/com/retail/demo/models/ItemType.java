package com.retail.demo.models;

public enum ItemType {
    GROCERY, FURNITURE, ELECTRONIC, CLOTHING, COSMETICS
}
