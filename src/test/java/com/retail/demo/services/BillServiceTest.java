package com.retail.demo.services;

import com.retail.demo.models.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;

public class BillServiceTest {

    private BillService discountService = new BillService();

    @Test
    public void calculateTotalPrice_forEmployee() {
        Employee employee = new Employee(1L, "firstName", "lastName");

        assertEquals(0.3, employee.getStoreDiscount(), 0);

        List<Item> items = new ArrayList<>();
        items.add(new Item(1L, "item1", "item1 description", ItemType.GROCERY, 10));
        items.add(new Item(2L, "item2", "item2 description", ItemType.GROCERY, 15));
        items.add(new Item(3L, "item3", "item3 description", ItemType.GROCERY, 5));
        items.add(new Item(4L, "item4", "item4 description", ItemType.ELECTRONIC, 100));
        items.add(new Item(5L, "item5", "item5 description", ItemType.CLOTHING, 50));

        Bill bill = new Bill(1L, items);

        // total price for groceries: 30
        // total price for non groceries: 150
        // total price after 0.03 discount on non groceries: 150 * 0.07 = 105
        // total price after ($5 for every $100) discount: (105 + 30) - 5 = 130
        assertEquals(130, discountService.calculateTotalPrice(bill.getItems(), employee.getStoreDiscount()), 0);
    }

    @Test
    public void calculateTotalPrice_forAffiliate() {
        Affiliate affiliate = new Affiliate(1L, "firstName", "lastName");

        assertEquals(0.1, affiliate.getStoreDiscount(), 0);

        List<Item> items = new ArrayList<>();
        items.add(new Item(1L, "item1", "item1 description", ItemType.FURNITURE, 300));
        items.add(new Item(2L, "item2", "item2 description", ItemType.FURNITURE, 500));
        items.add(new Item(3L, "item3", "item3 description", ItemType.COSMETICS, 30));

        Bill bill = new Bill(1L, items);

        // total price for groceries: 0
        // total price for non groceries: 830
        // total price after 0.1 discount on non groceries: 830 * 0.9 = 747
        // total price after ($5 for every $100) discount: 747 - 35 = 712
        assertEquals(712, discountService.calculateTotalPrice(bill.getItems(), affiliate.getStoreDiscount()), 0);
    }

    @Test
    public void calculateTotalPrice_forNewCustomer() {
        Customer customer = new Customer(1L, "firstName", "lastName");

        assertEquals(0, customer.getStoreDiscount(), 0);

        List<Item> items = new ArrayList<>();
        items.add(new Item(1L, "item1", "item1 description", ItemType.GROCERY, 100));
        items.add(new Item(2L, "item2", "item2 description", ItemType.FURNITURE, 100));
        items.add(new Item(3L, "item3", "item3 description", ItemType.ELECTRONIC, 100));

        Bill bill = new Bill(1L, items);

        // total price for groceries: 100
        // total price for non groceries: 200
        // total price after ($5 for every $100) discount: (100 + 200) - 15 = 285
        assertEquals(285, discountService.calculateTotalPrice(bill.getItems(), customer.getStoreDiscount()), 100);
    }

    @Test
    public void calculateTotalPrice_forOldCustomer() {
        Customer customer = new Customer(1L, "firstName", "lastName");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -2);
        customer.setCustomerSince(cal.getTime());

        assertEquals(0.05, customer.getStoreDiscount(), 0);

        List<Item> items = new ArrayList<>();
        items.add(new Item(1L, "item1", "item1 description", ItemType.GROCERY, 100));
        items.add(new Item(2L, "item2", "item2 description", ItemType.FURNITURE, 100));
        items.add(new Item(3L, "item3", "item3 description", ItemType.ELECTRONIC, 100));

        Bill bill = new Bill(1L, items);

        // total price for groceries: 100
        // total price for non groceries: 200
        // total price after 0.03 discount on non groceries: 200 * 0.95 = 190
        // total price after ($5 for every $100) discount: (190 + 100) - 10 = 280
        assertEquals(280, discountService.calculateTotalPrice(bill.getItems(), customer.getStoreDiscount()), 0);
    }
}